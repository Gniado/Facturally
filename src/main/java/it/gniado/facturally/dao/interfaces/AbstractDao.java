package it.gniado.facturally.dao.interfaces;

import java.util.List;

import it.gniado.facturally.model.AbstractModel;

public interface AbstractDao<T extends AbstractModel> {

	Long persist(T entity);

	void delete(T entity);

	List<T> findAll();

	T update(T entity);
}
