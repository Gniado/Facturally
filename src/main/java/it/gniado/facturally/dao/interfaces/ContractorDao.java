package it.gniado.facturally.dao.interfaces;

import javax.ejb.Local;

import it.gniado.facturally.model.Contractor;

@Local
public interface ContractorDao extends AbstractDao<Contractor> {

	Contractor findById(Long id);

	void delete(Long id);

}
