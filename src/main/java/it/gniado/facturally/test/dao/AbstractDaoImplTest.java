package it.gniado.facturally.test.dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import it.gniado.facturally.dao.ContractorDaoImpl;
import it.gniado.facturally.model.Address;
import it.gniado.facturally.model.Contractor;
import it.gniado.facturally.test.PersitenceConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { PersitenceConfiguration.class, ContractorDaoImpl.class })
@Transactional
public class AbstractDaoImplTest {

	@PersistenceContext
	private EntityManager em;

	@Inject
	private ContractorDaoImpl dao;

	@Test
	public void testPersist() throws Exception {
		Address address = new Address();
		address.setStreet("Słoneczna");
		address.setHouseNumber("40");
		address.setApartmentNumber("13");
		address.setPostalCode("82-300");
		address.setCity("Elbląg");

		Contractor contractor = new Contractor();
		contractor.setName("Gniado IT - Bartosz Gniado");
		contractor.setBankAccount("11 2222 3333 4444 5555 6666");
		contractor.setIdentityNumber("5783017082");
		contractor.setAddress(address);

		dao.persist(contractor);
	}
}